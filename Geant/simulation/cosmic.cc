/* 

Copyright (c) 2007-2012, The Regents of the University of California. 
Produced at the Lawrence Livermore National Laboratory 
UCRL-CODE-227323. 
All rights reserved. 
 
For details, see http://nuclear.llnl.gov/simulations
Please also read this http://nuclear.llnl.gov/simulations/additional_bsd.html
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
 
1.  Redistributions of source code must retain the above copyright
notice, this list of conditions and the disclaimer below.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the disclaimer (as noted below) in
the documentation and/or other materials provided with the
distribution.

3. Neither the name of the UC/LLNL nor the names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OF
THE UNIVERSITY OF CALIFORNIA, THE U.S. DEPARTMENT OF ENERGY OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


//******************************************************************************
// cosmic.cc  GEANT4 user application for testing handling of
//            cosmic-rays
//
// 1.00 JMV, LLNL, JAN-2007:  First version.
//******************************************************************************
//

// misc includes
//
#include <fstream>
#include <math.h>
#include <string>
#include "G4ios.hh"

// package includes
//
#include "G4RunManager.hh"
#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"

// geant4 includes
//
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "QBBC.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include "RunAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"


//------------------------------------------------------------------------------
int main(int argc, char** argv) {

  std::cout << std::stol(argv[1]) << " " << argv[1] << std::endl;

  CLHEP::HepRandom::setTheSeed(std::stol(argv[1]));
  std::cout << "-----------------------------0-----------------------------" << std::endl;
  // Run manager
  //------------
  G4RunManager * theRunManager = new G4RunManager;

  // UserInitialization classes
  //---------------------------
  DetectorConstruction* theDetector = new DetectorConstruction;
  //PhysicsList* thePhysicsList = new PhysicsList;

  // UserAction classes
  //-------------------

  theRunManager->SetUserInitialization(theDetector);
  
  std::cout << "-----------------------------1-----------------------------" << std::endl;

  G4VModularPhysicsList* physicsList = new QBBC;
  physicsList->SetVerboseLevel(1);
  theRunManager->SetUserInitialization(physicsList);
  //theRunManager->SetUserInitialization(thePhysicsList);

  std::cout << "-----------------------------2-----------------------------" << std::endl;

  auto generator = new PrimaryGeneratorAction("");

  theRunManager->SetUserAction(generator);

  std::cout << "-----------------------------3-----------------------------" << std::endl;
  
  auto runAction = new B1::RunAction;
  theRunManager->SetUserAction(runAction);

  std::cout << "-----------------------------4-----------------------------" << std::endl;

  auto eventAction = new B1::EventAction(runAction);
  theRunManager->SetUserAction(eventAction);

  std::cout << "-----------------------------5-----------------------------" << std::endl;

  B1::SteppingAction *stepAction = new B1::SteppingAction(eventAction, std::stoi(argv[1]));
  
  std::cout << stepAction->btlHits << std::endl;

  theRunManager->SetUserAction(stepAction);

  std::cout << "-----------------------------6-----------------------------" << std::endl;
  std::cout << stepAction->btlHits << std::endl;


  // Initialize G4 kernel
  //---------------------
  theRunManager->Initialize();

  std::cout << "-----------------------------7-----------------------------" << std::endl;
  std::cout << stepAction->btlHits << std::endl;


  G4VisManager* visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();

  std::cout << "-----------------------------8-----------------------------" << std::endl;
  std::cout << stepAction->btlHits << std::endl;



    // User interactions
  //------------------
  G4UImanager * UI = G4UImanager::GetUIpointer();  

  std::cout << "-----------------------------9-----------------------------" << std::endl;
  std::cout << stepAction->btlHits << std::endl;


  G4UIExecutive* ui = nullptr;
  if ( argc == 1 ) { ui = new G4UIExecutive(argc, argv); }

  G4String command = "/control/execute ";
  G4String fileName = "crySetup.mac";
  UI->ApplyCommand(command+fileName);

  std::cout << "----------------------------10-----------------------------" << std::endl;
  std::cout << stepAction->btlHits << std::endl;
  std::ofstream recordTime;
  int totalEvents = 0;
  G4String runCommand = "/run/beamOn 500000";
  UI->ApplyCommand(runCommand);
  //recordTime.open("CosmicTimeData.csv");i
  /*for(int i = 0; i<500; i++){ 
      std::cout << "Starting step " << i+1 << " of 500" << std::endl;
      
  }*/
  /*
  do {
    //std::cout << "Starting beam again." << std::endl;
    UI->ApplyCommand(runCommand);
    totalEvents+=1000;
    
    recordTime << generator->getTime() << ","<< stepAction->checkHits()<< std::endl;
    std::cout << totalEvents << " Events Simulated" << std::endl;
  } while(stepAction->checkContinue());
  recordTime.close();
  */
  //UI->ApplyCommand("/run/beamOn 100");

  //UI->ApplyCommand(runCommand);

  /*
  if ( ! ui ) {
    // batch mode
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UI->ApplyCommand(command+fileName);
  }
  else {
    // interactive mode
    UI->ApplyCommand("/control/execute init_vis.mac");
    ui->SessionStart();
    delete ui;
  }



  
  if (argc > 1){  //....geant command file specified on command line
    UI->ApplyCommand("/control/execute "+G4String(argv[1]));

  }else{           //....no command file specified, Start interactive session 
    G4UIsession * theUIsession = new G4UIterminal(new G4UItcsh);
    theUIsession->SessionStart();
    delete theUIsession;
  }
  */




  delete theRunManager;
  return EXIT_SUCCESS;
}
