//******************************************************************************
// DetectorConstruction.hh
//
// 1.00 RMK, LANL, MAR-2002:  First version.
//******************************************************************************
//
#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

class G4VPhysicalVolume;

#include "G4VUserDetectorConstruction.hh"
#include "G4UniformMagField.hh"
#include "G4SystemOfUnits.hh"
#include <iostream>

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    DetectorConstruction() = default;
    ~DetectorConstruction() override = default;

    //void ConstructSDandField() override;
    G4VPhysicalVolume* Construct() override;

    //static G4ThreadLocal G4GlobalMagFieldMessenger*  fMagFieldMessenger;

    G4LogicalVolume* GetScoringVolume() const { return fScoringVolume; }

    G4MagneticField* magField;

  protected:
    G4LogicalVolume* fScoringVolume = nullptr;
};


class MyField : public G4MagneticField{
    void GetFieldValue(const double Point[4], double *field) const {
        double r2 = Point[1]*Point[1] + (Point[2]+8000)*(Point[2]+8000);
        field[1] = 0;
        field[2] = 0;
        
        if(r2<(3000*3000)){
          field[0] = 3.8*tesla;
          //std::cout << field[0] << ", " << Point[1] << ", " << Point[2]+8000 << std::endl;
        } else if(r2<(5000*5000)) {
          field[0] = -2.0*tesla;
        } else {
          field[0] = 0;
          //std::cout << field[0] << ", " << Point[1] << ", " << Point[2]+8000 << std::endl;
        }
    }
};


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
