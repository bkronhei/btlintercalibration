//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B1/src/DetectorConstruction.cc
/// \brief Implementation of the B1::DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

namespace B1
{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();

  // Envelope parameters
  //
  G4double env_sizeXY = 50*mm, env_sizeZ = 40*mm;
  G4Material* env_mat = nist->FindOrBuildMaterial("G4_WATER");

  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //
  // World
  //
  G4double world_sizeX = 8*60*mm;
  G4double world_sizeY = 8*10*mm;
  G4double world_sizeZ  = 3*m;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");

  auto solidWorld = new G4Box("World",                           // its name
    0.5 * world_sizeX, 0.5 * world_sizeY, 0.5 * world_sizeZ);  // its size

  auto logicWorld = new G4LogicalVolume(solidWorld,  // its solid
    world_mat,                                       // its material
    "World");                                        // its name

  auto physWorld = new G4PVPlacement(nullptr,  // no rotation
    G4ThreeVector(),                           // at (0,0,0)
    logicWorld,                                // its logical volume
    "World",                                   // its name
    nullptr,                                   // its mother  volume
    false,                                     // no boolean operation
    0,                                         // copy number
    checkOverlaps);                            // overlaps checking

  
  auto Lu = nist->FindOrBuildMaterial("G4_Lu");
  auto Y = nist->FindOrBuildMaterial("G4_Y");
  auto Si = nist->FindOrBuildMaterial("G4_Si");
  auto O = nist->FindOrBuildMaterial("G4_O");
  auto Ce = nist->FindOrBuildMaterial("G4_Ce");

  auto LYSO = new G4Material("LYSO", 7.25*g/cm3, 5);
  LYSO->AddMaterial(Lu, 73.856*perCent);
  LYSO->AddMaterial(Y, 1.975*perCent);
  LYSO->AddMaterial(Si, 6.240*perCent);
  LYSO->AddMaterial(O, 17.773*perCent);
  LYSO->AddMaterial(Ce, 0.156*perCent);


  G4MaterialPropertiesTable *mpt = new G4MaterialPropertiesTable();


  const G4int num = 20;

  G4double ene[num]   =  {1.79*eV, 1.85*eV, 1.91*eV, 1.97*eV,
                          2.04*eV, 2.11*eV, 2.19*eV, 2.27*eV,
                          2.36*eV, 2.45*eV, 2.56*eV, 2.67*eV,
                          2.80*eV, 2.94*eV, 3.09*eV, 3.25*eV,
                          3.44*eV, 3.65*eV, 3.89*eV, 4.16*eV};



  G4double fast[num]  =  {0.01, 0.10, 0.20, 0.50,
                          0.90, 1.70, 2.90, 5.00,
                          8.30, 12.5, 17.0, 22.9,
                          26.4, 25.6, 16.8, 4.20,
                          0.30, 0.20, 0.10, 0.01};



  G4double rLyso[num] =  {1.81, 1.81, 1.81, 1.81,
                          1.81, 1.81, 1.81, 1.81,
                          1.81, 1.81, 1.81, 1.81,
                          1.81, 1.81, 1.81, 1.81,
                          1.81, 1.81, 1.81, 1.81};




  G4double abs[num]   =  {3.5*m, 3.5*m, 3.5*m, 3.5*m,
                          3.5*m, 3.5*m, 3.5*m, 3.5*m,
                          3.5*m, 3.5*m, 3.5*m, 3.5*m,
                          3.5*m, 3.5*m, 3.5*m, 3.5*m,
                          3.5*m, 3.5*m, 3.5*m, 3.5*m};



  mpt->AddProperty("SCINTILLATIONCOMPONENT1", ene, fast, num);
  mpt->AddProperty("RINDEX", ene, rLyso , num);
  mpt->AddProperty("ABSLENGTH", ene, abs, num);
  mpt->AddConstProperty("SCINTILLATIONYIELD1",32/keV);
  mpt->AddConstProperty("RESOLUTIONSCALE", 1);
  mpt->AddConstProperty("SCINTILLATIONTIMECONSTANT1",41*ns);
  LYSO->SetMaterialPropertiesTable(mpt);
  
  /*

  //
  // Envelope
  //
  
  auto solidEnv = new G4Box("Envelope",                    // its name
    world_sizeX/2, world_sizeY/2, world_sizeZ/2);  // its size

  auto logicEnv = new G4LogicalVolume(solidEnv,  // its solid
    world_mat,                                     // its material
    "Envelope");                                 // its name




  new G4PVPlacement(nullptr,  // no rotation
    G4ThreeVector(),          // at (0,0,0)
    logicEnv,                 // its logical volume
    "Envelope",               // its name
    logicWorld,               // its mother  volume
    false,                    // no boolean operation
    0,                        // copy number
    checkOverlaps);           // overlaps checking
  */

  // Shape 1
  G4Material* shape1_mat = nist->FindOrBuildMaterial("LYSO");//"G4_Lu");

  // Trapezoid shape
  G4double shape1_dx = 57*0.5*mm;
  G4double shape1_dy = 16*3.5*0.5*mm;
  G4double shape1_dz = 3.5*0.5*mm;
  auto solidShape1 = new G4Box("Shape1",  // its name
    shape1_dx, shape1_dy, shape1_dz);  // its size

  auto logicShape1 = new G4LogicalVolume(solidShape1,  // its solid
    shape1_mat,                                        // its material
    "Shape1");                                         // its name


  auto solidShape2 = new G4Box("Shape2",  // its name
    shape1_dy, shape1_dx, shape1_dz);  // its size

  auto logicShape2 = new G4LogicalVolume(solidShape2,  // its solid
    shape1_mat,                                        // its material
    "Shape2");                                         // its name

  G4RotationMatrix* rm = new G4RotationMatrix();
  rm->rotateY(-52.*deg);
  G4ThreeVector pos0 = G4ThreeVector(0*mm, 0*mm, 0.25*m);//-3.5*2*shape1_dz);
  new G4PVPlacement(rm     ,  // rotation
    pos0,                     // at position
    logicShape1,              // its logical volume
    "Test Module",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    0,                        // copy number
    checkOverlaps);           // overlaps checking


  G4ThreeVector pos1 = G4ThreeVector(0*mm, 0*mm, 0*m);//-3.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos1,                     // at position
    logicShape2,              // its logical volume
    "Reference Module",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    0,                        // copy number
    checkOverlaps);           // overlaps checking

  

  /*
  G4ThreeVector pos1 = G4ThreeVector(0*mm, 0*mm, -2.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos1,                     // at position
    logicShape1,              // its logical volume
    "Shape2",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    1,                        // copy number
    checkOverlaps);           // overlaps checking


  G4ThreeVector pos2 = G4ThreeVector(0*mm, 0*mm, -1.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos2,                     // at position
    logicShape1,              // its logical volume
    "Shape3",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    2,                        // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos3 = G4ThreeVector(0*mm, 0*mm, -0.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos3,                     // at position
    logicShape1,              // its logical volume
    "Shape4",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    3,                       // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos4 = G4ThreeVector(0*mm, 0*mm, 0.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos4,                     // at position
    logicShape1,              // its logical volume
    "Shape5",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    4,                        // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos5 = G4ThreeVector(0*mm, 0*mm, 1.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos5,                     // at position
    logicShape1,              // its logical volume
    "Shape6",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    5,                        // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos6 = G4ThreeVector(0*mm, 0*mm, 2.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos6,                     // at position
    logicShape1,              // its logical volume
    "Shape7",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    6,                        // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos7 = G4ThreeVector(0*mm, 0*mm, 3.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos7,                     // at position
    logicShape1,              // its logical volume
    "Shape8",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    7,                        // copy number
    checkOverlaps);           // overlaps checking

  */
  
  /*
  new G4PVParameterised(
    "Shape1",                 // its name
    logicShape1,              // its logical volume
    logicWorld,               // its mother  volume
    kZAxis,                   //param axix
    8,                        // n copies
    false,                    // pParam
    checkOverlaps);           // overlaps checking
  */
  // Set Shape2 as scoring volume
  //
  fScoringVolume = logicShape1;

  //
  //always return the physical World
  //
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}