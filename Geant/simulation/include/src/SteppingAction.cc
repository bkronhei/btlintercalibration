//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B1/src/SteppingAction.cc
/// \brief Implementation of the B1::SteppingAction class

#include <iostream>
#include <fstream>
#include <string>

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4ThreeVector.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"

namespace B1
{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(EventAction* eventAction)
: fEventAction(eventAction)
{
  std::string name = "Geant4Data.txt";
  recordFile.open(name.c_str());
}

SteppingAction::~SteppingAction()
{
  recordFile.close();//open('Geant4Data.txt')
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void SteppingAction::record(const G4Step* step){
    const G4Track *track = step->GetTrack();
    
    const G4StepPoint *preStep = step->GetPreStepPoint();
    const G4ThreeVector prePostion = preStep->GetPosition();
    

    const G4StepPoint *postStep = step->GetPostStepPoint();
    const G4ThreeVector postPostion = postStep->GetPosition();
    
    recordFile << track->GetTrackID();
    recordFile << ", " << track->GetParticleDefinition()->GetParticleName();
    recordFile << ", " << track->GetDynamicParticle()->GetMass();
    recordFile << ", " << step->GetTotalEnergyDeposit();
    recordFile << "," << track->GetVolume()->GetName();
    recordFile << ", " << prePostion.x() << ", " << prePostion.y() << ", " << prePostion.z() << ", " << preStep->GetGlobalTime() << ", " << preStep->GetKineticEnergy();
    recordFile << ", " << postPostion.x() << ", " << postPostion.y() << ", " << postPostion.z() << ", " << postStep->GetGlobalTime() << ", " << postStep->GetKineticEnergy() << std::endl;
}

void SteppingAction::UserSteppingAction(const G4Step* step)
{
  
  record(step);


  if (!fScoringVolume) {
    const auto detConstruction = static_cast<const DetectorConstruction*>(
      G4RunManager::GetRunManager()->GetUserDetectorConstruction());
    fScoringVolume = detConstruction->GetScoringVolume();
  }

  // get volume of the current step
  G4LogicalVolume* volume
    = step->GetPreStepPoint()->GetTouchableHandle()
      ->GetVolume()->GetLogicalVolume();

  // check if we are in scoring volume
  if (volume != fScoringVolume) return;

  // collect energy deposited in this step
  G4double edepStep = step->GetTotalEnergyDeposit();
  fEventAction->AddEdep(edepStep);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}