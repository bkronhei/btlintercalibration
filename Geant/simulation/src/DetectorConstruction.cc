//******************************************************************************
// DetectorConstruction.cc
//
// 1.00 JMV, LLNL, MAR-2002:  First version.
//******************************************************************************
//
#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4UniformMagField.hh"
#include "G4TransportationManager.hh"
#include "G4FieldManager.hh"

#include "globals.hh"

//DetectorConstruction::DetectorConstruction()
//{;}

//DetectorConstruction::~DetectorConstruction()
//{;}

/*void DetectorConstruction::ConstructSDandField()
{
  
  // Create global magnetic field messenger.
  // Uniform magnetic field is then created automatically if
  // the field value is not zero.
  //G4ThreeVector fieldValue = G4ThreeVector();
  //fMagFieldMessenger = new G4GlobalMagFieldMessenger(fieldValue);
  //fMagFieldMessenger->SetVerboseLevel(1);

  // Register the field messenger for deleting
  //G4AutoDelete::Register(fMagFieldMessenger);
  
}*/

//void DetectorConstruction::ConstructSDandField(){}


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();

  // Envelope parameters
  //
  G4double env_sizeXY = 50*mm, env_sizeZ = 40*mm;
  G4Material* env_mat = nist->FindOrBuildMaterial("G4_WATER");

  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //
  // World
  //
  G4double world_sizeX = 10*m;//*57*0.5*mm;
  G4double world_sizeY = 15*m;//*16*3.5*0.5*mm;
  G4double world_sizeZ  = 30*m;//*3.5*0.5*mm;

  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");

  auto solidWorld = new G4Box("World",                           // its name
    0.5 * world_sizeX, 0.5 * world_sizeY, 0.5 * world_sizeZ);  // its size

  auto logicWorld = new G4LogicalVolume(solidWorld,  // its solid
    world_mat,                                       // its material
    "World");                                        // its name

  auto physWorld = new G4PVPlacement(nullptr,  // no rotation
    G4ThreeVector(),                           // at (0,0,0)
    logicWorld,                                // its logical volume
    "World",                                   // its name
    nullptr,                                   // its mother  volume
    false,                                     // no boolean operation
    0,                                         // copy number
    checkOverlaps);                            // overlaps checking

  
  auto Lu = nist->FindOrBuildMaterial("G4_Lu");
  auto Y = nist->FindOrBuildMaterial("G4_Y");
  auto Si = nist->FindOrBuildMaterial("G4_Si");
  auto O = nist->FindOrBuildMaterial("G4_O");
  auto Ce = nist->FindOrBuildMaterial("G4_Ce");
  auto Cu = nist->FindOrBuildMaterial("G4_Cu");
  auto Zn = nist->FindOrBuildMaterial("G4_Zn");

  auto shape1_mat = new G4Material("LYSO", 7.125*g/cm3, 5);
  shape1_mat->AddMaterial(Lu, 71.813*perCent);
  shape1_mat->AddMaterial(Y, 3.613*perCent);
  shape1_mat->AddMaterial(Si, 6.338*perCent);
  shape1_mat->AddMaterial(O, 18.046*perCent);
  shape1_mat->AddMaterial(Ce, 0.19*perCent);


  auto shape3_mat = new G4Material("BRASS", 8.73*g/cm3, 2);
  shape3_mat->AddMaterial(Cu, 70.0*perCent);
  shape3_mat->AddMaterial(Zn, 30.0*perCent);




  /*
  G4MaterialPropertiesTable *mpt = new G4MaterialPropertiesTable();


  const G4int num = 20;

  G4double ene[num]   =  {1.79*eV, 1.85*eV, 1.91*eV, 1.97*eV,
                          2.04*eV, 2.11*eV, 2.19*eV, 2.27*eV,
                          2.36*eV, 2.45*eV, 2.56*eV, 2.67*eV,
                          2.80*eV, 2.94*eV, 3.09*eV, 3.25*eV,
                          3.44*eV, 3.65*eV, 3.89*eV, 4.16*eV};



  G4double fast[num]  =  {0.01, 0.10, 0.20, 0.50,
                          0.90, 1.70, 2.90, 5.00,
                          8.30, 12.5, 17.0, 22.9,
                          26.4, 25.6, 16.8, 4.20,
                          0.30, 0.20, 0.10, 0.01};



  G4double rLyso[num] =  {1.81, 1.81, 1.81, 1.81,
                          1.81, 1.81, 1.81, 1.81,
                          1.81, 1.81, 1.81, 1.81,
                          1.81, 1.81, 1.81, 1.81,
                          1.81, 1.81, 1.81, 1.81};




  G4double abs[num]   =  {3.5*m, 3.5*m, 3.5*m, 3.5*m,
                          3.5*m, 3.5*m, 3.5*m, 3.5*m,
                          3.5*m, 3.5*m, 3.5*m, 3.5*m,
                          3.5*m, 3.5*m, 3.5*m, 3.5*m,
                          3.5*m, 3.5*m, 3.5*m, 3.5*m};



  mpt->AddProperty("SCINTILLATIONCOMPONENT1", ene, fast, num);
  mpt->AddProperty("RINDEX", ene, rLyso , num);
  mpt->AddProperty("ABSLENGTH", ene, abs, num);
  mpt->AddConstProperty("SCINTILLATIONYIELD1",32/keV);
  mpt->AddConstProperty("RESOLUTIONSCALE", 1);
  mpt->AddConstProperty("SCINTILLATIONTIMECONSTANT1",41*ns);
  
  
  LYSO->SetMaterialPropertiesTable(mpt);
  */
  /*

  //
  // Envelope
  //
  
  auto solidEnv = new G4Box("Envelope",                    // its name
    world_sizeX/2, world_sizeY/2, world_sizeZ/2);  // its size

  auto logicEnv = new G4LogicalVolume(solidEnv,  // its solid
    world_mat,                                     // its material
    "Envelope");                                 // its name




  new G4PVPlacement(nullptr,  // no rotation
    G4ThreeVector(),          // at (0,0,0)
    logicEnv,                 // its logical volume
    "Envelope",               // its name
    logicWorld,               // its mother  volume
    false,                    // no boolean operation
    0,                        // copy number
    checkOverlaps);           // overlaps checking
  */

  // Shape 1
  //G4Material* shape1_mat = nist->FindOrBuildMaterial("LYSO");//"G4_Lu");
  G4Material* shape2_mat = nist->FindOrBuildMaterial("G4_PbWO4");//"G4_Lu");
  //G4Material* shape3_mat = nist->FindOrBuildMaterial("BRASS");//"G4_Lu");

  // Trapezoid shape
  G4double shape1_dx = 0.5*m;//57*0.5*m;
  G4double shape1_dy = 0.5*m;//16*3.5*0.5*mm;
  G4double shape1_dz = 0.05*m;//3.5*0.5*mm;
  /*
  auto solidShape1 = new G4Box("Shape1",  // its name
    shape1_dx, shape1_dy, shape1_dz);  // its size

  auto logicShape1 = new G4LogicalVolume(solidShape1,  // its solid
    shape1_mat,                                        // its material
    "Shape1");                                         // its name


  auto solidShape2 = new G4Box("Shape2",  // its name
    shape1_dy, shape1_dx, shape1_dz);  // its size

    auto logicShape2 = new G4LogicalVolume(solidShape2,  // its solid
    shape1_mat,                                        // its material
    "Shape2");                                         // its name


  */
  
  int trayNumber = 36;
  int zSegment = 12*8;
  int phiSegment = 3;
  int barSegment = 16;

  G4double BTLLength = 5.2*m;
  G4double phiLength = 0.184*m;
  G4double barModuleLength = (2*2.6/zSegment)*m;

  G4double tray_dz = (0.184/phiSegment)*m;   // Length in phi
  G4double tray_dy = 0.0035*m;  // Transverse length
  G4double tray_dx = (2*2.6/zSegment/barSegment)*m;     // Length in z

  G4double zOffset = -8.0*m;
  G4double yOffset = 0.0*m;
  G4double xOffset = 0.0*m;
  
  G4double radius = 1.148*m + (0.0035/2)*m;
  
  auto solidShapeTray = new G4Box("BTLTray",  // its name
    tray_dx/2, tray_dy/2, tray_dz/2);  // its size

  auto logicShapeTray = new G4LogicalVolume(solidShapeTray,  // its solid
    shape1_mat,                                        // its material
    "BTLTray"); 

  /*for(int i = 0; i<trayNumber; i++){
      for(int j=0; j<zSegment; j++){
        G4RotationMatrix* trayRm = new G4RotationMatrix();
        G4ThreeVector trayPos = G4ThreeVector(0*mm, radius, 0*mm).rotateX(i*10.0*deg);
        trayRm->rotateX(-i*10.0*deg);
        trayPos[0] += xOffset - BTLLength/2 + tray_dx*(1+2*j)/2;
        trayPos[1] += yOffset;
        trayPos[2] += zOffset;
        std::string trayName = "BTL_Tray_"+std::to_string(i*zSegment+j)+"_";
        new G4PVPlacement(trayRm,       // rotation
            trayPos,                    // at position
            logicShapeTray,             // its logical volume
            trayName.c_str(),           // its name
            logicWorld,                 // its mother  volume
            false,                      // no boolean operation
            0,                          // copy number
            checkOverlaps);             // overlaps checking
      }
  }*/


  for(int i = 0; i<trayNumber; i++){
      for(int j=0; j<zSegment; j++){
          for(int k=0; k<phiSegment; k++){
              for(int l=0; l<barSegment; l++){
                  G4RotationMatrix* trayRm = new G4RotationMatrix();
                  G4ThreeVector trayPos = G4ThreeVector(- barModuleLength/2 + tray_dx*(1+2*l)/2,
                                                         radius,
                                                        - phiLength/2 + tray_dz*(1+2*k)/2).rotateX(i*10.0*deg);
                  trayRm->rotateX(-i*10.0*deg);
                  trayPos[0] += xOffset - BTLLength/2 + barSegment*tray_dx*(1+2*j)/2;
                  trayPos[1] += yOffset;
                  trayPos[2] += zOffset;
                  std::string trayName = std::to_string(i*zSegment*phiSegment*barSegment+j*phiSegment*barSegment+k*barSegment+l);
                  new G4PVPlacement(trayRm,       // rotation
                      trayPos,                    // at position
                      logicShapeTray,             // its logical volume
                      trayName.c_str(),           // its name
                      logicWorld,                 // its mother  volume
                      false,                      // no boolean operation
                      0,                          // copy number
                      false);             // overlaps checking

              }
          }
      }
  }

  


  /*

  auto solidShape1 = new G4Tubs("Shape1",  // its name
    1.148*m, 1.153*m,2.6*m, 0.*deg,360.*deg);  // its size

  auto logicShape1 = new G4LogicalVolume(solidShape1,  // its solid
    shape1_mat,                                        // its material
    "Vol1"); 


  
  G4RotationMatrix* rm = new G4RotationMatrix();
  rm->rotateY(90.*deg);
  G4ThreeVector pos1 = G4ThreeVector(0*mm, 0*mm, -8*m);//-3.5*2*shape1_dz);
  new G4PVPlacement(rm     ,  // rotation
    pos1,                     // at position
    logicShape1,              // its logical volume
    "BTL",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    0,                        // copy number
    checkOverlaps);           // overlaps checking



  */

  
  G4RotationMatrix* rm = new G4RotationMatrix();
  rm->rotateY(90.*deg);
  
  
  auto solidShape2 = new G4Tubs("Shape2",  // its name
    1.29*m, 1.52*m,2.6*m, 0.*deg,360.*deg);  // its size

  auto logicShape2 = new G4LogicalVolume(solidShape2,  // its solid
    shape2_mat,                                        // its material
    "Vol2"); 


  G4ThreeVector pos2 = G4ThreeVector(0*mm, 0*mm, -8*m);//-3.5*2*shape1_dz);
  new G4PVPlacement(rm     ,  // rotation
    pos2,                     // at position
    logicShape2,              // its logical volume
    "ECAL",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    0,                        // copy number
    checkOverlaps);           // overlaps checking


  auto solidShape3 = new G4Tubs("Shape3",  // its name
    1.775*m, 2.8765*m,2.6*m, 0.*deg,360.*deg);  // its size

  auto logicShape3 = new G4LogicalVolume(solidShape3,  // its solid
    shape3_mat,                                        // its material
    "Vol3"); 


  G4ThreeVector pos3 = G4ThreeVector(0*mm, 0*mm, -8*m);//-3.5*2*shape1_dz);
  new G4PVPlacement(rm     ,  // rotation
    pos3,                     // at position
    logicShape3,              // its logical volume
    "HCAL",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    0,                        // copy number
    checkOverlaps);           // overlaps checking

  
  
  /*

  G4ThreeVector pos1 = G4ThreeVector(0*mm, 0*mm, 0*m);//-3.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos1,                     // at position
    logicShape2,              // its logical volume
    "Reference Module",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    0,                        // copy number
    checkOverlaps);           // overlaps checking

  */

  /*
  G4ThreeVector pos1 = G4ThreeVector(0*mm, 0*mm, -2.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos1,                     // at position
    logicShape1,              // its logical volume
    "Shape2",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    1,                        // copy number
    checkOverlaps);           // overlaps checking


  G4ThreeVector pos2 = G4ThreeVector(0*mm, 0*mm, -1.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos2,                     // at position
    logicShape1,              // its logical volume
    "Shape3",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    2,                        // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos3 = G4ThreeVector(0*mm, 0*mm, -0.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos3,                     // at position
    logicShape1,              // its logical volume
    "Shape4",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    3,                       // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos4 = G4ThreeVector(0*mm, 0*mm, 0.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos4,                     // at position
    logicShape1,              // its logical volume
    "Shape5",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    4,                        // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos5 = G4ThreeVector(0*mm, 0*mm, 1.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos5,                     // at position
    logicShape1,              // its logical volume
    "Shape6",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    5,                        // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos6 = G4ThreeVector(0*mm, 0*mm, 2.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos6,                     // at position
    logicShape1,              // its logical volume
    "Shape7",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    6,                        // copy number
    checkOverlaps);           // overlaps checking

    G4ThreeVector pos7 = G4ThreeVector(0*mm, 0*mm, 3.5*2*shape1_dz);
  new G4PVPlacement(nullptr,  // no rotation
    pos7,                     // at position
    logicShape1,              // its logical volume
    "Shape8",                 // its name
    logicWorld,                 // its mother  volume
    false,                    // no boolean operation
    7,                        // copy number
    checkOverlaps);           // overlaps checking

  */
  
  /*
  new G4PVParameterised(
    "Shape1",                 // its name
    logicShape1,              // its logical volume
    logicWorld,               // its mother  volume
    kZAxis,                   //param axix
    8,                        // n copies
    false,                    // pParam
    checkOverlaps);           // overlaps checking
  */
  // Set Shape2 as scoring volume
  //
  fScoringVolume = logicShapeTray;

  //G4MagneticField* magField = new G4UniformMagField(G4ThreeVector(0.,0.,3.8*tesla));
  G4MagneticField* magField = new MyField();//new G4UniformMagField(G4ThreeVector(3.8*tesla,0.,0.));
  G4FieldManager* globalFieldMrg = G4TransportationManager::GetTransportationManager()->GetFieldManager();
  globalFieldMrg->SetDetectorField(magField);
  globalFieldMrg->CreateChordFinder(magField);
  
  //
  //always return the physical World
  //
  return physWorld;
}

