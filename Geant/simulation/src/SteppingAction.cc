//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B1/src/SteppingAction.cc
/// \brief Implementation of the B1::SteppingAction class

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4ThreeVector.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"

namespace B1
{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(EventAction* eventAction, int file)
: fEventAction(eventAction)
{
  int bars = 12*36*24*16;
  std::string name = "Geant4Data" + std::to_string(file) + ".dat";
  btlHits = new std::vector<int>();
  btlHits->resize(bars);
  std::cout <<"-------------------------------a-----------------------------------------" <<std::endl;
  for(int i=0;i<bars;i++){
    btlHits->at(i) = 0;
  }
  std::cout << btlHits << std::endl;
  std::cout << btlHits->size() << std::endl;
  std::cout <<"b" <<std::endl;
  recordFile = fopen(name.c_str() ,"wb");
}

SteppingAction::~SteppingAction()
{
  fclose(recordFile);//open('Geant4Data.txt')
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void SteppingAction::record(const G4Step* step){
    

    //std::cout << "----------------------------10.1-----------------------------" << std::endl;
    const G4Track *track = step->GetTrack();

    
    
    //std::cout << "----------------------------10.2-----------------------------" << std::endl;
    if(track->GetTrackID()==1){
      

      //double eventNumber = 

      const G4StepPoint *preStep = step->GetPreStepPoint();
      const G4ThreeVector prePostion = preStep->GetPosition();
      

      const G4StepPoint *postStep = step->GetPostStepPoint();
      const G4ThreeVector postPostion = postStep->GetPosition();
      
      //recordFile << track->GetTrackID();
      //recordFile << ", " << track->GetParticleDefinition()->GetParticleName();
      //recordFile << ", " << track->GetDynamicParticle()->GetMass();
      double recordData [13] = {};
      
      recordData[0] = (double) G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
      recordData[1] = step->GetTotalEnergyDeposit();
      std::string volumeName = track->GetVolume()->GetName();
      if(volumeName=="World"){
          recordData[2] = -1;
      } else if (volumeName=="ECAL") {
          recordData[2] = -2;
      } else if (volumeName=="HCAL") {
        recordData[2] = -3;
      } else {
        recordData[2] = (double)std::stoi(volumeName);
      }

      /*std::string nextVolumeName = track->GetNextVolume()->GetName();
      if(nextVolumeName=="World"){
          recordData[3] = -1;
      } else if (nextVolumeName=="ECAL") {
          recordData[3] = -2;
      } else if (nextVolumeName=="HCAL") {
        recordData[3] = -3;
      } else {
        recordData[3] = (double)std::stoi(nextVolumeName);
      }*/

      recordData[3] = prePostion.x();
      recordData[4] = prePostion.y();
      recordData[5] = prePostion.z();
      recordData[6] = preStep->GetGlobalTime();
      recordData[7] = preStep->GetKineticEnergy();
      recordData[8] = postPostion.x();
      recordData[9] = postPostion.y();
      recordData[10] = postPostion.z();
      recordData[11] = postStep->GetGlobalTime();
      recordData[12] = postStep->GetKineticEnergy();
      

      if(recordData[2]>=0){// || recordData[3]>=0){
          fwrite(recordData, sizeof(double), 13, recordFile);
      }

      //recordFile << ", " << prePostion.x() << ", " << prePostion.y() << ", " << prePostion.z() << ", " << preStep->GetGlobalTime() << ", " << preStep->GetKineticEnergy();
      //recordFile << ", " << postPostion.x() << ", " << postPostion.y() << ", " << postPostion.z() << ", " << postStep->GetGlobalTime() << ", " << postStep->GetKineticEnergy() << std::endl;
      
      //std::cout << "----------------------------10.3-----------------------------" << std::endl;
      if(track->GetTrackID()==1 && track->GetVolume()->GetName()[0]=='B' && step->GetTotalEnergyDeposit() > 0.5){
        std::string num = "";
        int i = 9;
        //std::cout << "----------------------------10.4-----------------------------" << std::endl;
        while(track->GetVolume()->GetName()[i]!='_'){
          num+=track->GetVolume()->GetName()[i];
          i++;
        } 
        //std::cout << "----------------------------10.5-----------------------------" << std::endl;
        int hitLoc = std::stoi(num);
        
        btlHits->at(hitLoc) = btlHits->at(hitLoc)+1;
      }
    }
    //std::cout << "----------------------------10.6-----------------------------" << std::endl;
}

bool SteppingAction::checkContinue(){
    //bool returnVal = false;
    //int i = 0;
    //int modulesHit = 0;
    for(int hits : *btlHits){
        
        if(hits==0){
          //std::cout << i << std::endl;
          return(true);
        } 
        //else {
        //  modulesHit++;
        //}
        //i++;
    }

    //std::cout << modulesHit << " modules of " << btlHits->size() << " have been hit." << std::endl;

    return(false);
}


int SteppingAction::checkHits(){
    //int i = 0;
    int modulesHit = 0;
    for(int hits : *btlHits){
        
        if(hits!=0){
          modulesHit++;
        }
        //i++;
    }

    return(modulesHit);
}

void SteppingAction::UserSteppingAction(const G4Step* step)
{
  
  record(step);


  if (!fScoringVolume) {
    const auto detConstruction = static_cast<const DetectorConstruction*>(
      G4RunManager::GetRunManager()->GetUserDetectorConstruction());
    fScoringVolume = detConstruction->GetScoringVolume();
  }

  // get volume of the current step
  G4LogicalVolume* volume
    = step->GetPreStepPoint()->GetTouchableHandle()
      ->GetVolume()->GetLogicalVolume();

  // check if we are in scoring volume
  if (volume != fScoringVolume) return;

  // collect energy deposited in this step
  G4double edepStep = step->GetTotalEnergyDeposit();
  fEventAction->AddEdep(edepStep);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

}
