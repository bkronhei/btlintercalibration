"""
Collect all the simulation data and move it to the calibration folder
"""

import numpy as np
import os

allData = []

for fileName in os.listdir(): # files in current folder
        if(len(fileName)>4 and fileName[-4:]==".dat"): # All files ending in .dat
                data = np.fromfile(fileName, dtype=np.float64)
                allData.append(data)
                cols = 13 # 13 columns per example
                rows = len(data)//cols # determine number of rows
                data = data.reshape((rows, cols))

allData = np.reshape(np.concatenate(allData), -1)
allData.tofile("../../calibration/mergedData.dat") # save to calibration folder

