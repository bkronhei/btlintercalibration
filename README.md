# BTLInterCalibration
This is a code repository containing code to perform a crude simulation of the BTL and some other pieces of CMS for incident cosmic muons. This simulation is used to determine the feasibility of interaclibrating the LYSO bars of the BTL with them.


## Simulation

### Simulation fidelity
The simulation used is not a perfect representation of CMS, merely a rough copy of parts relavent to whether cosmic muons hit the BTL. Only the BTL, ECAL, and HCAL are included in the simulation. The ECAL is included as a single piece of lead tungstate and the HCAL as a single piece of brass. The magnetic field is included somewhat crudely, going straight from 3.8 tesla to -2 to 0. The BTL istself consists of all 

### Package installation
This simulation was designed to run with a GEANT4 and CRY installation outside of CMSSW on a CMS Tier 3 cluster. As such, users will need to either install GEANT4 separately [here](https://geant4.web.cern.ch/download/11.1.2.html) or determine a way to make the CMSSW version work. Similarly, CRY can be installed [here](https://nuclear.llnl.gov/simulation/main.html). Note that the setup files for these will need to be run in order for their installations to work. A possible example of this is:

```
source /home/yourUsername/Geant4/geant4-v11.1.2-install/bin/geant4.sh
source /home/yourUsername/cry_v1.7/setup
```

A working installation of CMake is also required. If this is not already present on the system being used, it can be installed [here](https://cmake.org/install/).

The only other package which needs to be installed is `numpy` in Python. If this is not installed it can be installed through

```
pip install numpy
```

### Project build
Once all the prior packages have been installed the simulation can be built. In the terminal, change directory into `btlintercalibration/Geant/simulation_build/`. In that directory run

```
source env.sh
cmake -DGeant4_DIR=/path/to/geant4-v11.1.2-install/lib/Geant4-11.1.2/ -DCMAKE_PREFIX_PATH=/path/to/geant4-v11.1.2-install/lib/cmake/Geant4 ../simulation
make -j8 cosmics
```

with `/path/to/` set to the appropriate values. Once this has run and compiled correctly the Condor submission file can be submitted. It is possible this may need to be adjusted depending on how Condor is setup. On the cluster used it took around 12 or less hours for each of the jobs to run.

### Simulation execution
To execute the simulation simply submit the Condor submission file

```
condor_submit condorSubmit.sub
```

As is, the file will run 320 jobs with 500,000 simulated events each. To change the number of jobs simply change the number after `queue` at the bottom of the file. Each execution uses a different random seed determined by the job id.

Once all the jobs have finished run

```
python collectData.py
```

in the same folder. This will combine all the output files and move them to the calibration folder.


## Calibration

The calibration stage has three steps. First, the data is processed into two datasets for the position and time calibration via 'extractData.cc'. Next, 'analyzeLocationData.cc' is used to determine the calibration of the location inside the bar. Finally, 'analyzeTimeData.cc' is used to determine the time error on each bar.

### Compilation
All three files need to be compiled. Simply run:

```
g++ extractData.cc $(root-config --libs) $(root-config --cflags) -O3 -o extract
g++ analyzeLocationData.cc $(root-config --libs) $(root-config --cflags) -O3 -o location
g++ analyzeTimeData.cc $(root-config --libs) $(root-config --cflags) -O3 -o time
```

Note that you will need to have root in the path for this to work, as well as a proper version of g++.

### Extract Data

This will process the data output from Geant. The form the Geant data data takes is:

| Column |            Content            |
| ------ | ----------------------------- |
|   0    | Event ID                      |
|   1    | Energy deposited (MeV)        |
|   2    | Bar Number                    |
|   3    | Previous x position (mm)      |
|   4    | Previous y position (mm)      |
|   5    | Previous z position (mm)      |
|   6    | Previous time (ns)            |
|   7    | Previous kinetic Energy (MeV) | 
|   8    | Next x postiion (mm)          |
|   9    | Next y position (mm)          |
|  10    | Next z position (mm)          |
|  11    | Next time (ns)                |
|  12    | Next kinetic energy (MeV)     |

Each row represents when a cosmic muon interacted with one of the BTL bars. Only steps where this occured are recorded. All values in this datset, as well as all others, are doubles.

When executing the extraction code there is a single command line argument: the minium energy required to consider a hit a hit. It is recommended to set this at 0.5 MeV. The code will then find all events where a muon interacted with at least two BTL bars and deposited 0.5 MeV into each. Various histograms will be created showing properties of the data. They will be prefixed by the energy cut. To execute, simply run

```
./extract 0.5
```

In addition to the histograms, it will make two binary files. Their content is

locationDataset.dat:

| Column |            Content            |
| ------ | ----------------------------- |
|   0    | Bar Number                    |
|   1    | Bar hit location              |


timeDataset.dat:

| Column |            Content            |
| ------ | ----------------------------- |
|   0    | First Bar Number              |
|   1    | Second Bar Number             |
|   2    | Time difference (ps)          |


### Location calibration

The location calibration is done by picking assigning a random miscalibration to each bar. For each event known to hit a bar, an additional error is added to the measurement corresponding to the BTL timing accuracy translated into distance inside the bar. These measurements of the error are accumulated and a measurement is recorded for each bar. The error is expected to be the BTL location accuracy divided by the square root of the number of measurements. The size of the miscalibration on each bar does not matter. Thus, to have the uncertainty on this calibration be 10% of the overall BTL uncertainty we need 100 hits.

The code takes two command line arguments. The first is the standard deviation of the errors in each bar in ps. 100 is a reasonable number. The second is the standard deviation for the noise on a given BTL poisition measurement. 60 ps is reasonable. The code is executed as follows:

```
./location 100 60
```

Several histogrmas showing the results will be output along with some text. It will also make a binary file of doubles. The first column is all the true errors, the second column is the predicted error.


### Timing Calibration

For timing calibration each bar is again assigned a fixed offset. Each measurement represents a comparison in the difference in time between two bars and the time of flight from the tracker. Thus, it comes from two time measurements, so its uncertainty is sqrt(2) times the time uncertiainty of a single bar. This results in a system of equations for the difference of the errors of different bars. This system can be solved via least squares, though a naive approach does not work as this produces an enormous 165,888 by 165,888 matrix.

If `A` is the matrix representing the system of equations, `b` is the target, and `x` is the input, then `ATAx - ATb` (where `AT` is `A` transpose), is the derivative of the squared error of `b-Ax`. Knowing this, the values of `ATA` times any vector can be calculated just using the system of equations stores as three arrays, which is much faster. This, combined with the conjugate gradient method for solving this type of problem, are used to calcualte the corrections. 

The exact values for the uncertainties on each prediction are not exactly measured from the fit, but they fit well with (measurementErrors)*sqrt(2/examples). When a muon hits three bars the number of examples for each bar hit is only one, despite the middle bar being involved in two equations. This choice helps ensure agreement between the prediction and data.

To run the code three command line arguments are necesary. The first is the standard deviation of the errors in each par in ps; 100 is reasonable. The second is the uncertainty of each measurement. 30 is reasonable. Finally, the last number is the error tolerance for the conjugate gradient method. 1e-8 works well. The code can be called as follows:

```
./time 100 30 1e-8
```

Several histogrmas showing the results will be output. It will also make a binary file of doubles. The first column is all the true errors, the second column is the predicted error.