/*
[0] = bar
[1] = position

shape: 2 x 4,675,910
*/

#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>

#include <random>
#include <fstream>
#include <iostream>
#include <vector>
#include <unordered_set>
#include <cmath>
#include <ctime>

#include <sys/stat.h>

#define EXAMPLES 4675910
#define BARS 165888
#define CLYSORECIP 17

int main(int argc, char **argv){

    std::time_t msStart = std::time(nullptr);

    // Give usage info
    if (argc < 3) {
        std::cerr << "----------------------------------------------------------------------------------------------------------" << std::endl;
        std::cerr << "Usage: " << argv[0] << " CalibrationError MeasurementError MaxFitError" << std::endl;
        std::cerr << "CalibrationError is the standard deviation of the errors on the individual crystals. 100 is reasonable." << std::endl;
        std::cerr << "MeasurementError is the standard deviation of the errors on each individual measurement. 60 is reasonable." << std::endl;
        std::cerr << "----------------------------------------------------------------------------------------------------------" << std::endl;
        return 1;
    }

    // File holding data
    std::string fileName = "locationDataset.dat";

    // Make histograms
    TH1F *measMeans = new TH1F("measMeans", "Measured mean", 100, -500,500);
    TH1F *trueMeans = new TH1F("trueMeans", "True mean", 100, -500,500);
    TH1F *meanErrors = new TH1F("meanErrors", "Errors in mean", 100, -50,50);
    TH1F *meanUncerts = new TH1F("meanUncerts", "Uncerts in mean", 100, 0,30);
    TH1F *newUncerts = new TH1F("newUncertaintiesTime", "New total Time Uncertainty", 100, 59,67);
    TH1F *newUncertsLoc = new TH1F("newUncertaintiesLocation", "New total Location Uncertainty", 100, 3.5,3.9);

    // Histogram display helpers
    std::string prefix = "loc_";
    TCanvas *canvas = new TCanvas();

    // Get the standard deviation of position miscalibation
    double errorSize = std::stod(argv[1]);

    // Get the precision of individual measurement
    double precisionSize = std::stod(argv[2]);

    // Setup random number generation
    std::mt19937_64 gen64;
    gen64.seed(42);
    std::normal_distribution<double> barDistribution(0.0,errorSize);
    std::normal_distribution<double> measurementDistribution(0.0,precisionSize);

    // Declare vectors holding useful information
    std::vector<int>    bars = std::vector<int>();
    std::vector<double> times = std::vector<double>();
    std::vector<double> timeErrors = std::vector<double>();
    std::vector<double> barErrors = std::vector<double>();
    std::vector<double> barMeans = std::vector<double>();
    std::vector<double> barMeanUncerts = std::vector<double>();
    
    std::vector<std::vector<double>> allMeasurements = std::vector<std::vector<double>>();

    // Resize vecotr appropriately
    bars.resize(EXAMPLES);
    times.resize(EXAMPLES);    
    timeErrors.resize(EXAMPLES);

    barErrors.resize(BARS);
    barMeans.resize(BARS);
    barMeanUncerts.resize(BARS);
    allMeasurements.resize(BARS);

    // hold data from file
    double *dataRead = new double[EXAMPLES*2];

    // Read the data
    std::ifstream myFile(fileName, std::fstream::binary);
    myFile.read((char *) dataRead, EXAMPLES*2*8);

    // Generate by bar information
    for(int i=0; i<BARS; i++){
        allMeasurements[i] = std::vector<double>();
        barErrors[i] = barDistribution(gen64);
    }

    for(int i=0; i<EXAMPLES; i++){      
        // Copy data from file holder
        bars[i] = (int) dataRead[2*i];
        times[i] = dataRead[2*i+1];

        // Get errors
        timeErrors[i] = measurementDistribution(gen64);
        timeErrors[i] += barErrors[bars[i]];
        allMeasurements[bars[i]].push_back(timeErrors[i]);
        
    }

    // variable which will hold max uncert in the mean at the end
    double maxMeanUncert = 0.0;

    // holds save data
    double *saveData = new double[BARS*2];

    for(int i=0; i<BARS; i++){

        // Calcualte the mean and standard deviation for each bar
        double sum = std::accumulate(std::begin(allMeasurements[i]), std::end(allMeasurements[i]), 0.0);
        double m =  sum / allMeasurements[i].size();
        double accum = 0.0;
        std::for_each (std::begin(allMeasurements[i]), std::end(allMeasurements[i]), [&](const double d) {
            accum += (d - m) * (d - m);
        });

        double stdev = sqrt(accum / (allMeasurements[i].size()-1));
        
        // Fill vectors
        barMeans[i] = m;
        barMeanUncerts[i] = stdev/sqrt(allMeasurements[i].size());

        // Fill histos
        meanErrors->Fill(m-barErrors[i]);
        meanUncerts->Fill(barMeanUncerts[i]);
        trueMeans->Fill(barErrors[i]);
        measMeans->Fill(m);

        saveData[i*2+0] = barErrors[i];
        saveData[i*2+1] = m;

        newUncerts->Fill(sqrt(3600.0+pow(barMeanUncerts[i], 2)));                // Uncert in time
        newUncertsLoc->Fill(sqrt(3600.0+pow(barMeanUncerts[i], 2))/CLYSORECIP);  // Uncert in position

        // Update max mean uncert
        if(barMeanUncerts[i] > maxMeanUncert){
            maxMeanUncert = barMeanUncerts[i];
        }

    }

    // Save predictions
    FILE *locPreds = fopen("locPredictions.dat", "wb");
    fwrite(saveData, sizeof(double), BARS*2, locPreds);
    fclose(locPreds);

    // Display histos
    std::string name = prefix + "TrueMeans.png";
    trueMeans->SetMinimum(0);
    trueMeans->Draw();
    trueMeans->GetXaxis()->SetTitle("True mean (ps)");
    canvas->Print(name.c_str());

    name = prefix + "MeasuredMeans.png";
    measMeans->SetMinimum(0);
    measMeans->Draw();
    measMeans->GetXaxis()->SetTitle("Measured mean (ps)");
    canvas->Print(name.c_str());

    name = prefix + "NewUncertainties.png";
    newUncerts->SetMinimum(0);
    newUncerts->Draw();
    newUncerts->GetXaxis()->SetTitle("New Uncertainties (ps)");
    canvas->Print(name.c_str());

    name = prefix + "NewUncertaintiesLoc.png";
    newUncertsLoc->SetMinimum(0);
    newUncertsLoc->Draw();
    newUncertsLoc->GetXaxis()->SetTitle("New  location uncertainties (mm)");
    canvas->Print(name.c_str());

    name = prefix + "MeanErrors.png";
    meanErrors->SetMinimum(0);
    meanErrors->Draw();
    meanErrors->GetXaxis()->SetTitle("Errors in mean (ps)");
    canvas->Print(name.c_str());

    name = prefix + "MeanUncerts.png";
    meanUncerts->SetMinimum(0);
    meanUncerts->Draw();
    meanUncerts->GetXaxis()->SetTitle("Uncert in mean (ps)");
    canvas->Print(name.c_str());

    // Print some useful information to the screen
    std::cout << std::endl;
    std::cout << "The starting error has a standard devation of " << argv[2] << " ps" << std::endl; 
    std::cout << "The maximum uncertainty on the location correction is " << maxMeanUncert << std::endl;
    std::cout << "This increased time uncertainty from " << precisionSize << " ps to " << sqrt(pow(precisionSize, 2)+pow(maxMeanUncert, 2)) << std::endl;
    std::cout << "This increased location uncertainty from " << precisionSize/CLYSORECIP << " mm to " << sqrt(pow(precisionSize, 2)+pow(maxMeanUncert, 2))/CLYSORECIP << std::endl;

    std::time_t msEnd = std::time(nullptr);
    std::cout << "Time elapsed: " << double(msEnd - msStart) << " seconds" << std::endl;

}
