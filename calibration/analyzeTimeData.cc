/*
[0] = first bar
[1] = second bar
[2] = time of flight

expected execution time: about 5 seconds
*/

#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraph.h>
#include <TLegend.h>

#include <random>
#include <fstream>
#include <iostream>
#include <vector>
#include <unordered_set>
#include <cmath>
#include <map>
#include <ctime>

#include <sys/stat.h>

#include "helpers.h"

#define EXAMPLES 4675910
#define BARS 165888


int main(int argc, char **argv){

    std::time_t msStart = std::time(nullptr);

    // Give usage info
    if (argc < 4) {
        std::cerr << "----------------------------------------------------------------------------------------------------------" << std::endl;
        std::cerr << "Usage: " << argv[0] << " CalibrationError MeasurementError MaxFitError" << std::endl;
        std::cerr << "CalibrationError is the standard deviation of the errors on the individual crystals. 100 is reasonable." << std::endl;
        std::cerr << "MeasurementError is the standard deviation of the errors on each individual measurement. 30 is reasonable." << std::endl;
        std::cerr << "MaxFitError is the maximum acceptable error on the least squares fit. 1e-8 is reasonable." << std::endl;
        std::cerr << "----------------------------------------------------------------------------------------------------------" << std::endl;
        return 1;
    }

    
    // Get the standard deviation of position miscalibation
    double errorSize = std::stod(argv[1]);

    // Get the precision of individual measurements
    double precisionSize = std::stod(argv[2]);

    // Get the maximum fit error
    double maxError = std::stod(argv[3]);

    // File holding data
    std::string fileName = "timeDataset.dat";


    // Make histograms
    TH1F *hitDistribution = new TH1F("histDist", "Distribtuion of Hits", 100, 0,100);
    TH1F *fistHitDistribution = new TH1F("histDistFirst", "Distribtuion of Hits", 100, 0,100);
    TH1F *secondDistribution = new TH1F("histDistSecond", "Distribtuion of Hits", 100, 0,100);
    TH1F *errors = new TH1F("errors", "Correction errors", 100, -precisionSize,precisionSize);
    TH1F *uncertHist = new TH1F("uncerts", "Fit uncerts", 25, 4,18);
    TH1F *totalUncertHist = new TH1F("totalUncerts", "Total Uncerts", 25, precisionSize,precisionSize*34/30);



    
    // Histogram display helpers
    std::string prefix = "time_";
    TCanvas *canvas = new TCanvas();


    // Setup random number generation
    std::mt19937_64 gen64;
    gen64.seed(42);
    std::normal_distribution<double> barDistribution(0.0,errorSize);
    std::normal_distribution<double> measurementDistribution(0.0,precisionSize);


    // Declare arrays holding useful information for analysis
    int *barsPrior     = new int[EXAMPLES];     // first bar hit
    int *barsPost      = new int[EXAMPLES];     // second bar hit
    double *times      = new double[EXAMPLES];  // time of flight
    double *timeErrors = new double[EXAMPLES];  // error in time of flight
    double *barErrors  = new double[BARS];      // constant error in each bar
    double *b          = new double[BARS];      // target for least squares later
    int *allHits       = new int[BARS];         // Number of total hits per bar
    int *firstHits     = new int[BARS];         // Number of total hits per bar from first hit
    int *secondHits    = new int[BARS];         // Number of total hits per bar from second hit


    // hold data from file
    double *dataRead = new double[EXAMPLES*3];

    // Read the data
    std::ifstream myFile(fileName, std::fstream::binary);
    myFile.read((char *) dataRead, EXAMPLES*3*8);

    
    // Generate by bar information
    for(int i=0; i<BARS; i++){
        barErrors[i] = barDistribution(gen64); // set bar error
        b[i] = 0.0;
        allHits[i] = 0.0;                     
        firstHits[i] = 0.0;
        secondHits[i] = 0.0;
    }

    
    for(int i=0; i<EXAMPLES; i++){      
        // Copy data from file holder
        barsPrior[i] = ((int) dataRead[3*i]);
        barsPost[i] = ((int) dataRead[3*i+1]);
        times[i] = dataRead[3*i+2];

        // Count hits
        if(i==0){
            allHits[barsPrior[i]]++;
        } else if(barsPost[i-1]!=barsPrior[i]) {
            allHits[barsPrior[i]]++;
        }
        allHits[barsPost[i]]++;

        firstHits[barsPrior[i]]++;
        secondHits[barsPost[i]]++;

        // Get errors
        timeErrors[i] = measurementDistribution(gen64)+measurementDistribution(gen64); // random measurement uncert on both bars
        timeErrors[i] += barErrors[barsPost[i]] - barErrors[barsPrior[i]];               // constant measurement error on each bar


        b[barsPrior[i]] -= timeErrors[i]*2; // ATb
        b[barsPost[i]]  += timeErrors[i]*2; // ATb   
    }

    // Fill bar hit histograms
    for(int i=0; i<BARS; i++){
        hitDistribution->Fill(allHits[i]);
        fistHitDistribution->Fill(firstHits[i]);
        secondDistribution->Fill(secondHits[i]);
    }

    // Draw bar hit histograms
    std::string name = prefix + "AllHits.png";
    hitDistribution->SetMinimum(0);
    hitDistribution->Draw();
    hitDistribution->GetXaxis()->SetTitle("Hits");
    hitDistribution->GetYaxis()->SetTitle("Number of bars");
    canvas->Print(name.c_str());

    name = prefix + "FirstHits.png";
    fistHitDistribution->SetMinimum(0);
    fistHitDistribution->Draw();
    fistHitDistribution->GetXaxis()->SetTitle("Hits");
    fistHitDistribution->GetYaxis()->SetTitle("Number of bars");
    canvas->Print(name.c_str());

    name = prefix + "SecondHits.png";
    secondDistribution->SetMinimum(0);
    secondDistribution->Draw();
    secondDistribution->GetXaxis()->SetTitle("Hits");
    secondDistribution->GetYaxis()->SetTitle("Number of bars");
    canvas->Print(name.c_str());
    

    // arrays holding information for conjugate gradient    
    double *x     = zeros(BARS);
    double *xNext = zeros(BARS);
    double *r     = copy(b, BARS);
    double *rNext = copy(b, BARS);
    double *p     = copy(b, BARS);
    double *pNext = copy(b, BARS);

    // constant for conjugate gradient
    double alpha = 0.0;
    double beta  = 0.0;


    // First calculations
    double *Ap           = aTaMult(p, barsPrior, barsPost, EXAMPLES, BARS);
    double pTApScalar    = dot(p, Ap, BARS);
    double rTrScalar     = dot(r,r, BARS);
    double rTrScalarNext = rTrScalar;


    // Start the conjugate gradient optimization
    std::cout << "Error step 0: " << sqrt(rTrScalar) << "\n";
    int step = 0;
    while(sqrt(rTrScalar)>maxError){ // Execute until desired error reached
        // Perform one iteration
        step++;
        alpha = rTrScalar/pTApScalar;
        addMult(x, p, xNext, alpha, BARS);
        addMult(r, Ap, rNext, -alpha, BARS);
        rTrScalarNext = dot(rNext, rNext, BARS);
        beta = rTrScalarNext/rTrScalar;
        addMult(rNext, p, pNext, beta, BARS);

        aTaMult(pNext, Ap, barsPrior, barsPost, EXAMPLES, BARS);
        pTApScalar = dot(pNext, Ap, BARS);
        
        std::cout << "Error step "<< step << ": " << sqrt(rTrScalarNext) << "\n";

        step++;

        // Perform a second iteration with current/next flipped to avoid copying
        alpha = rTrScalarNext/pTApScalar;

        addMult(xNext, pNext, x, alpha, BARS);
        addMult(rNext, Ap, r, -alpha, BARS);
        rTrScalar = dot(r, r, BARS);
        beta = rTrScalar/rTrScalarNext;
        addMult(r, pNext, p, beta, BARS);


        aTaMult(p, Ap, barsPrior, barsPost, EXAMPLES, BARS);
        pTApScalar = dot(p, Ap, BARS);
        std::cout << "Error step "<< step << ": " << sqrt(rTrScalar) << "\n";
    }
    
    // Save the correct bar errors and the predicted errors
    double *saveData = new double[BARS*2];
    std::map<int, std::vector<double>> errorsPerHit = *(new std::map<int, std::vector<double>>()); // Map between number of hits and vector with bar information
    
    for(int i=0; i<BARS; i++){
        saveData[i*2] = barErrors[i];
        saveData[i*2+1] = x[i];
        errors->Fill(barErrors[i]-x[i]);
        errorsPerHit[allHits[i]].push_back(barErrors[i]-x[i]);
        
    }

    FILE *timePreds = fopen("timePredictions.dat", "wb");
    fwrite(saveData, sizeof(double), BARS*2, timePreds);
    fclose(timePreds);

    
    double *hits    = new double[errorsPerHit.size()]; // holds number of hits in a bar
    double *means   = new double[errorsPerHit.size()]; // mean of errors with the same number of hits
    double *sds     = new double[errorsPerHit.size()]; // standard deviation of erros with the same number of hits
    double *sdsPred = new double[errorsPerHit.size()]; // predicted standard deviation, sqrt(2)*singleHitUncert/sqrt(numberHits)
    int index = 0;
    for(auto keyValuePair = errorsPerHit.begin(); keyValuePair!=errorsPerHit.end(); keyValuePair++){ // iterate over map which was sorted
        if(keyValuePair->second.size()<2){ // Don't use vecotrs with less than 2 hits
            continue;
        }
        hits[index] = keyValuePair->first;

        // Calcualte the mean and standard deviation for each number of hits
        double sum = std::accumulate(std::begin(keyValuePair->second), std::end(keyValuePair->second), 0.0);
        double m =  sum / keyValuePair->second.size();
        double accum = 0.0;
        std::for_each (std::begin(keyValuePair->second), std::end(keyValuePair->second), [&](const double d) {
            accum += (d - m) * (d - m);
        });

        double stdev = sqrt(accum / (keyValuePair->second.size()-1));
        means[index] = m;
        sds[index] = stdev;
        sdsPred[index] = sqrt(2.0*pow(precisionSize,2))/sqrt(hits[index]);
        
        
        // Fill histograms with the total time uncertainty
        for(int i = 0; i<keyValuePair->second.size(); i++){
            uncertHist->Fill(sdsPred[index]);
            totalUncertHist->Fill(sqrt(precisionSize*precisionSize+sdsPred[index]*sdsPred[index]));
        }
        index++;
    }


    
    // Make graphs of the error data

    name = prefix + "Errors.png";
    errors->SetMinimum(0);
    errors->Draw();
    errors->GetXaxis()->SetTitle("Error in correction (ps)");
    errors->GetYaxis()->SetTitle("Number of bars");
    canvas->Print(name.c_str());


    name = prefix + "Uncert.png";
    uncertHist->SetMinimum(0);
    uncertHist->Draw("HIST");
    uncertHist->GetXaxis()->SetTitle("Uncertainty in correction (ps)");
    uncertHist->GetYaxis()->SetTitle("Number of bars");
    canvas->Print(name.c_str());

    name = prefix + "TotalUncert.png";
    totalUncertHist->SetMinimum(0);
    totalUncertHist->Draw("HIST");
    totalUncertHist->GetXaxis()->SetTitle("Total Uncertainty (ps)");
    totalUncertHist->GetYaxis()->SetTitle("Number of bars");
    canvas->Print(name.c_str());

    canvas->Clear();
    name = prefix + "Means.png";
    TGraph *meansGraph = new TGraph(index,hits,means);
    meansGraph->SetTitle("Mean error vs. Number of Hits");
    meansGraph->SetLineColor(kRed);
    meansGraph->Draw("");
    meansGraph->GetXaxis()->SetTitle("Hits per bar");
    meansGraph->GetYaxis()->SetTitle("Mean error (ps)");
    canvas->Print(name.c_str());

    
    canvas->Clear();
    
    name = prefix + "SDs.png";
    TGraph *sdsGraph = new TGraph(index,hits,sds);
    TGraph *sdsPredGraph = new TGraph(index,hits,sdsPred);
    sdsGraph->SetTitle("Standard Deviation vs. Number of Hits");
    
    sdsGraph->SetLineColor(kRed);
    sdsGraph->Draw("");
    
    sdsPredGraph->SetLineColor(kBlue);
    sdsPredGraph->Draw("SAME");
    
    sdsGraph->GetXaxis()->SetTitle("Hits per bar");
    sdsGraph->GetYaxis()->SetTitle("Standard devaition of error (ps)");
    
    TLegend * legendME = new TLegend(0.6,0.6, 0.9, 0.9);
    legendME->AddEntry(sdsGraph, "Measured", "L");
	legendME->AddEntry(sdsPredGraph, "Precision*Sqrt(2/examples)", "L");
	legendME->Draw();
    
    canvas->Print(name.c_str());

    // Display the time elapsed
    std::time_t msEnd = std::time(nullptr);
    std::cout << "Time elapsed: " << double(msEnd - msStart) << " seconds" << std::endl;

}
