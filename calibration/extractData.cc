/*
[0] = GetEventID();
[1] = step->GetTotalEnergyDeposit();
[2] = barNumber;
[3] = prePostion.x();
[4] = prePostion.y();
[5] = prePostion.z();
[6] = preStep->GetGlobalTime();
[7] = preStep->GetKineticEnergy();
[8] = postPostion.x();
[9] = postPostion.y();
[10] = postPostion.z();
[11] = postStep->GetGlobalTime();
[12] = postStep->GetKineticEnergy();
*/

#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>

#include <fstream>
#include <iostream>
#include <vector>
#include <unordered_set>
#include <cmath>
#include <ctime>

#include <sys/stat.h>


template <typename T>
class processData{
    private:
        std::vector<std::vector<T>> *data;
        std::vector<int> *eventStarts;
        std::vector<int> *eventSizes;
        std::vector<int> *eventUniqueHits;
        std::vector<int> *barHits;

        std::vector<T> *previousBar;
        std::vector<T> *newBar;
        std::vector<T> *previousMomentum;
        std::vector<T> *currentMomentum;
        std::vector<T> *timeDiff;
        std::vector<T> *trackTimeUncert;

        std::vector<T> *barHitNumber;
        std::vector<T> *barHitLoc;
        std::vector<T> *barHitMomentum;
        

        std::string prefix;
        TCanvas *canvas;
        TH1F *hitDistro;
        TH1F *timeDistroSmall;
        TH1F *timeDistroLarge;
        TH1F *barLocHist;
        TH1F *barMomentumHist;
        TH1F *phiHist;
        TH1F *zHist;
        TH1F *previousMomentumHist;
        TH1F *postMomentumHist;
        TH1F *momentumDiffHist;
        TH1F *timeUncertHist;
        TH2F *zPhiHist;

        std::string fileName;

        T energyCut;
        int hitCut;
        int numColumns;


        bool checkSkip(T *currentData){ 
            if(currentData[1]<energyCut){ // check if sufficient energy was deposited to show up
                return(true);
            }
            return(false);
        }



        void readFile(){
            // Determine the size of the file
            struct stat results;
            stat(fileName.c_str(), &results);
            int readSize = results.st_size;
            std::cout << "Reading file of size " << readSize << " bytes" << std::endl;
            
            // Size of array we are reading
            int arraySize = (int) readSize/sizeof(T);
            int numRows = (int) arraySize/numColumns;
            std::cout << "Array size is " << arraySize << std::endl;
            std::cout << "Row size is " << numRows << std::endl;
            
            // hold the data we are reading
            T *dataRead = new T[arraySize];

            // Read the data
            std::ifstream myFile(fileName, std::fstream::binary);
            myFile.read((char *) dataRead, readSize);
            
            // Make vectors to hold columns
            for(int i=0; i<numColumns; i++){
                std::vector<T> temp = *(new std::vector<T>());
                temp.resize(numRows);
                data->push_back(temp);
            }

            // Start copying data into vectors
            std::cout << "Copying" << std::endl;
            int currentRow = -1;
            int overwriteRows = 0;
            for(int i=0; i<arraySize; i++){
                int currentColumn = i%numColumns;
                if(currentColumn==0){
                    if(checkSkip(&dataRead[i])){  // Don't copy data which fails cuts
                        i+=numColumns-1;
                        currentRow+=1;
                        overwriteRows+=1;
                        continue;
                    }
                    currentRow+=1;

                }
                data->at(currentColumn).at(currentRow-overwriteRows) = dataRead[i];
            }

            // Resize vectors down to correct size
            for(int i=0; i<numColumns; i++){
                data->at(i).resize(numRows-overwriteRows);
            }

            std::cout << "New row size is " << numRows - overwriteRows << std::endl;

            // Free memory
            myFile.close();
            delete dataRead;
        }




        void getEvents(){
            std::cout << "Logging events" << std::endl;
            
            T previousEvents = 0;  // Number of events run in Geant so far
            int eventSize = 1;     // Starting at second event, so event size starts at 1
            int eventStart = 0;    // Index of first event
            std::unordered_set<T> eventHits = std::unordered_set<T>{data->at(2)[0]};
            // Set to hold hits in each event
            
            //for(int i=1; i<=100; i++){
            //    std::cout << this->hitDistro->GetBinCenter(i) << std::endl;
            //}
            

            for(int i=1; i<data->at(0).size();i++){
                data->at(0)[i]+=previousEvents;
                // Check if event number went down or stayed the same// but time went down
                if(data->at(0)[i]<data->at(0)[i-1]){// || (data->at(0)[i]==data->at(0)[i-1] && data->at(6)[i]>data->at(6)[i-1] )){ 
                    previousEvents+=500000; // Another 1000 events were processed
                    data->at(0)[i]+=500000;
                }

                if(data->at(0)[i] == data->at(0)[i-1]){ // Check if we are in the same event
                    eventSize++;
                    eventHits.insert(data->at(2)[i]);
                } else { // New event has started
                    if(eventHits.size()>=hitCut){ // If we had at least two hits, save the event
                        eventStarts->push_back(eventStart);
                        eventSizes->push_back(eventSize);
                        eventUniqueHits->push_back(eventHits.size());
                        
                        float tempHits = (float)eventHits.size(); 
 
                        // Reset event variables
                        eventStart = i;
                        eventSize = 1;
                        eventHits.clear();
                        eventHits.insert(data->at(2)[i]);
                        
                    }
                }
            }
        }


        void checkHits(){
            
            std::unordered_set<T> set = std::unordered_set<T>{};
            std::cout << "Number of events: " << eventStarts->size() << std::endl; // Number of pasing events
            
            for(int index=0; index<eventStarts->size(); index++){ // Add bars of all pasing hits to set
                for(int hit=0; hit<eventSizes->at(index); hit++){
                    set.insert(data->at(2)[hit+eventStarts->at(index)]);
                    barHits->at((int)data->at(2)[hit+eventStarts->at(index)]) +=1;
                    if(hit>=1){
                        // Record time difference in histos
                        timeDistroSmall->Fill(1000*data->at(11)[hit+eventStarts->at(index)]-1000*data->at(11)[hit-1+eventStarts->at(index)]);
                        timeDistroLarge->Fill(1000*data->at(11)[hit+eventStarts->at(index)]-1000*data->at(11)[hit-1+eventStarts->at(index)]);

                        // Record bar information, time information, and momentum information
                        previousBar->push_back(data->at(2)[hit-1+eventStarts->at(index)]);
                        newBar->push_back(data->at(2)[hit+eventStarts->at(index)]);
                        previousMomentum->push_back(data->at(12)[hit-1+eventStarts->at(index)]);
                        currentMomentum->push_back(data->at(7)[hit+eventStarts->at(index)]);


                        previousMomentumHist->Fill(data->at(12)[hit-1+eventStarts->at(index)]/1000);
                        postMomentumHist->Fill(data->at(7)[hit+eventStarts->at(index)]/1000);
                        momentumDiffHist->Fill(data->at(12)[hit-1+eventStarts->at(index)]- data->at(7)[hit+eventStarts->at(index)]);
                        
                        // TODO: look into why time can be negative without the fabs
                        double time = fabs(1000*data->at(11)[hit+eventStarts->at(index)]-1000*data->at(11)[hit-1+eventStarts->at(index)]);
                        double energy = (data->at(12)[hit-1+eventStarts->at(index)]+data->at(7)[hit+eventStarts->at(index)])/2;
                        
                        double energyUncert = 0.015;

                        double mass = 105.7; // MeV
                        double beta = sqrt(1-1/(pow((energy/mass+1),2)));
                        double betaUp = sqrt(1-1/(pow((energy*(1.0+energyUncert)/mass+1),2)));
                        double betaDown = sqrt(1-1/(pow((energy*(1.0-energyUncert)/mass+1),2)));
                        double fracUncert = fabs(betaUp-betaDown)/2/beta;

                        timeDiff->push_back(time);
                        
                        if(time<0){
                            std::cout << time << std::endl;
                        }

                        trackTimeUncert->push_back(fracUncert*time);
                        timeUncertHist->Fill(trackTimeUncert->back());

                    }
                    double pi = 3.14159265;
                    double x = (data->at(3)[hit+eventStarts->at(index)]);
                    double y = (data->at(4)[hit+eventStarts->at(index)]);
                    double z = (data->at(5)[hit+eventStarts->at(index)]);
                    z+=8000;
                    int barIndex = (int)data->at(2)[hit+eventStarts->at(index)];
                    
                    int trayNumber = 36;
                    int zSegment = 12*8;
                    int phiSegment = 3;
                    int barSegment = 16;
                    
                    int barNum = (barIndex%barSegment); 
                    barIndex = (barIndex-barNum)/barSegment;
                    
                    int phiNum = (barIndex%phiSegment); 
                    barIndex = (barIndex-phiNum)/phiSegment;

                    int zNum = (barIndex%zSegment); 
                    int trayNum  = (barIndex-zNum)/zSegment;


                    int iPhi = phiNum+trayNum*3;

                    phiHist->Fill(iPhi);
                    
                    zHist->Fill(x/1000);

                    zPhiHist->Fill(x/1000, iPhi);

                    double angle = atan2(z, y)*180/pi;
                    

                    angle = fmod(angle, 10);
                    if(angle<0){
                        angle+=10;
                    }
                    

                    if(angle>5){
                        angle = angle-10;
                    }

                    double r = 1148.0;
                    double barLength = 184.0/3.0;
                    double momentum = data->at(7)[hit+eventStarts->at(index)];

                    double rPrime = sqrt(pow(y,2)+pow(z,2));

                    double barDist = fmod(sin(angle*pi/180)*rPrime+barLength*1.5,barLength);

                    barHitLoc->push_back(barDist);
                    barHitNumber->push_back(data->at(2)[hit+eventStarts->at(index)]);
                    barLocHist->Fill(barHitLoc->back());
                    barHitMomentum->push_back(momentum);
                    barMomentumHist->Fill(barHitMomentum->back()/1000);
                }
            }

            // Display number of bars in set, should be 165,888 if all were hit
            std::cout << "Number of unique hits: " << set.size() << std::endl;
        }



        void makeHistos(){
            
            for(int i : *barHits){
                hitDistro->Fill(i);
            }

            std::string name = prefix + "_HitDistribution.png";
            hitDistro->SetMinimum(0);
            hitDistro->Draw();
            hitDistro->GetXaxis()->SetTitle("Hits in bar");
            canvas->Print(name.c_str());

            name = prefix + "_TimeDistributionSmall.png";
            timeDistroSmall->SetMinimum(0);
            timeDistroSmall->Draw();
            timeDistroSmall->GetXaxis()->SetTitle("Time between hits (ps)");
            canvas->Print(name.c_str());

            name = prefix + "_TimeDistributionLarge.png";
            timeDistroLarge->SetMinimum(0);
            timeDistroLarge->Draw();
            timeDistroLarge->GetXaxis()->SetTitle("Time between hits (ps)");
            canvas->Print(name.c_str());


            name = prefix + "_TimeUncert.png";
            timeUncertHist->SetMinimum(0);
            timeUncertHist->Draw();
            timeUncertHist->GetXaxis()->SetTitle("Time uncertainty (ps)");
            canvas->Print(name.c_str());

            

            name = prefix + "_BarLoc.png";
            barLocHist->SetMinimum(0);
            barLocHist->Draw();
            barLocHist->GetXaxis()->SetTitle("location (mm)");
            canvas->Print(name.c_str());


            name = prefix + "_BarMomentum.png";
            barMomentumHist->SetMinimum(0);
            barMomentumHist->Draw();
            barMomentumHist->GetXaxis()->SetTitle("momentum (GeV)");
            canvas->SetLogx();
            canvas->Print(name.c_str());


            name = prefix + "_PreviousMomentum.png";
            previousMomentumHist->SetMinimum(0);
            previousMomentumHist->Draw();
            previousMomentumHist->GetXaxis()->SetTitle("momentum (GeV)");
            canvas->Print(name.c_str());


            name = prefix + "_PostMomentum.png";
            postMomentumHist->SetMinimum(0);
            postMomentumHist->Draw();
            postMomentumHist->GetXaxis()->SetTitle("momentum (GeV)");
            canvas->Print(name.c_str());


            canvas->SetLogx(false);

            name = prefix + "_MomentumDiff.png";
            momentumDiffHist->SetMinimum(0);
            momentumDiffHist->Draw();
            momentumDiffHist->GetXaxis()->SetTitle("momentum (MeV)");
            canvas->Print(name.c_str());

            name = prefix + "_phi.png";
            phiHist->SetMinimum(0);
            phiHist->Draw();
            phiHist->GetXaxis()->SetTitle("iPhi");
            canvas->Print(name.c_str());


            name = prefix + "_z.png";
            zHist->SetMinimum(0);
            zHist->Draw();
            zHist->GetXaxis()->SetTitle("z distance (m)");
            canvas->Print(name.c_str());


            name = prefix + "_zPhi.png";
            zPhiHist->Draw("COLZ");
            zPhiHist->GetXaxis()->SetTitle("z distance (m)");
            zPhiHist->GetYaxis()->SetTitle("iPhi");
            zPhiHist->SetStats(false);
            canvas->Print(name.c_str());
            

        }
    
    
    
    public:
        processData(std::string fileName, int numColumns, double energyCut, int hitCut){
            
            // General information
            this->numColumns = numColumns;
            this->energyCut = energyCut;
            this->hitCut = hitCut;
            this->fileName = fileName;

            int momentumBinNumber = 200;
            float momentumBinMin = -2.0;
            float momentumBinMax = 3.0;

            float * momentumBins = new float[momentumBinNumber+1];
            for(int i=0; i<momentumBinNumber+1; i++){
                momentumBins[i] = pow(10,momentumBinMin + ((momentumBinMax-momentumBinMin)/momentumBinNumber)*i);
            }

            // Plotting objects
            canvas = new TCanvas();
            prefix = std::to_string(energyCut).substr(0,4);
            int hitDistroMax = 60;
            hitDistro = new TH1F("hitDistro", "Hit distribution", hitDistroMax+1, 0.0-0.5,hitDistroMax+0.5);
            timeDistroSmall = new TH1F("timeDistroSmall", "Time distribution", 200, 0,0.06*1000);
            timeDistroLarge = new TH1F("timeDistroLarge", "Time distribution", 200, 0.06*1000,30*1000);
            previousMomentumHist = new TH1F("previousMomentum", "Previous Momentum", momentumBinNumber, momentumBins);
            postMomentumHist = new TH1F("postMomentum", "Post Momentum", momentumBinNumber, momentumBins);
            momentumDiffHist = new TH1F("momentumDiff", "Momentum Difference", 200, -5, 5);
            timeUncertHist = new TH1F("timeUncert", "Time Uncertainty", 200, 0, 1);

            barLocHist = new TH1F("barLoc", "Location in bar", 50, 0, 61.33334);
            barMomentumHist = new TH1F("barMomentum", "Momentum in bar", momentumBinNumber, momentumBins);
            phiHist = new TH1F("iPhi", "Angle around BTL", 108, 0, 108);
            zHist = new TH1F("z", "Z distance along BTL", 1536, -2.6, 2.6);
            zPhiHist = new TH2F("zPhi", "z-iPhi location", 1536, -2.6, 2.6, 108, 0, 108);

            // basic data
            data = new std::vector<std::vector<T>>();
            
            // event information
            eventStarts = new std::vector<int>();
            eventSizes = new std::vector<int>();
            eventUniqueHits = new std::vector<int>();
            
            // bar hit information
            barHits = new std::vector<int>();
            barHits->resize(165888);

            // Relative timing information
            previousBar = new std::vector<T>;      // Save
            newBar = new std::vector<T>;           // Save
            previousMomentum = new std::vector<T>;
            currentMomentum = new std::vector<T>;
            timeDiff = new std::vector<T>;         // Save
            trackTimeUncert = new std::vector<T>;

            

            // hit location in bar
            barHitNumber = new std::vector<T>;     // Save
            barHitLoc = new std::vector<T>;        // Save
            barHitMomentum = new std::vector<T>;

            
            

            readFile();
            std::cout << std::endl;
            getEvents();
            std::cout << std::endl;
            checkHits();
            makeHistos();

            std::cout << std::endl;
            std::cout << "Saving data" << std::endl;

            double *timeDataSet = new double[previousBar->size()*3];
            for(int i=0; i<previousBar->size(); i++){
                timeDataSet[i*3]   = previousBar->at(i);
                timeDataSet[i*3+1] = newBar->at(i);
                timeDataSet[i*3+2] = timeDiff->at(i);
            }

            FILE *timeDS = fopen("timeDataset.dat", "wb");
            fwrite(timeDataSet, sizeof(double), previousBar->size()*3, timeDS);
            fclose(timeDS);

            double *locationDataSet = new double[barHitNumber->size()*2];

            for(int i=0; i<barHitNumber->size(); i++){
                locationDataSet[i*2]   = barHitNumber->at(i);
                locationDataSet[i*2+1] = barHitLoc->at(i);
            }

            FILE *locDS = fopen("locationDataset.dat", "wb");
            fwrite(locationDataSet, sizeof(double), barHitNumber->size()*2, locDS);
            fclose(locDS);
        }


};



int main(int argc, char **argv){

    std::time_t msStart = std::time(nullptr);

    // Give usage info
    if (argc < 2) {
        std::cerr << "----------------------------------------------------------------------------------------------------------" << std::endl;
        std::cerr << "Usage: " << argv[0] << " EnergyCut" << std::endl;
        std::cerr << "EnergyCut is the minimum accepted energy deposition in MeV. 0.5 is reasonable." << std::endl;
        std::cerr << "----------------------------------------------------------------------------------------------------------" << std::endl;
        return 1;
    }

    std::string fileName = "mergedData.dat";
    int numColumns = 13;
    int hitCut = 2;
    
    processData<double> data = processData<double>("mergedData.dat", numColumns, std::stod(argv[1]), hitCut);

    std::time_t msEnd = std::time(nullptr);
    std::cout << "Time elapsed: " << double(msEnd - msStart) << " seconds" << std::endl;

}
