// store a + b in c
void add(const double *a, const double *b, double *c, int entries){
    for(int i=0; i<entries; i++ ){
        c[i] = a[i] + b[i];
    }
}

// store a + b in a
void add(double *a, const double *b, int entries){
    for(int i=0; i<entries; i++ ){
        a[i] += b[i];
    }
}

// returns pointer to array of 0s of size entries
double * zeros(int entries){
    double *a = new double[entries];
    for(int i=0; i<entries; i++ ){
        a[i] = 0;
    }
    return(a);
}

// zeros a vector
void zeros(double *a, int entries){
    for(int i=0; i<entries; i++ ){
        a[i] = 0;
    }
}

// returns pointer to array of 1s of size entries
double * ones(int entries){
    double *a = new double[entries];
    for(int i=0; i<entries; i++ ){
        a[i] = 1;
    }
    return(a);
}

// make a duplicate of a and return a pointer to it
double * copy(const double *a, int entries){
    double *b = new double[entries];
    for(int i=0; i<entries; i++ ){
        b[i] = a[i];
    }
    return(b);
}

// copy a to b
void copy(const double *a, double *b, int entries){
    for(int i=0; i<entries; i++ ){
        b[i] = a[i];
    }
}

// bPrime = ATA * b
// first and second hits store the hits from the simulation or data
// aEntries is the number of examples
// bEntries is the number of bars
void aTaMult(const double *b, double *bPrime, int *firstHits, int *secondHits, int aEntries, int bEntries){
    for(int i=0; i<bEntries; i++ ){ // zero out bPrime
        bPrime[i] = 0;
    }
    for(int i=0; i<aEntries; i++){
        int row = firstHits[i];
        int column = secondHits[i];
        bPrime[row] += 2*(b[row] - b[column]);
        bPrime[column] += 2*(b[column] - b[row]);
    }
}

// Allocate bPrime
// bPrime = ATA * b
// first and second hits store the hits from the simulation or data
// aEntries is the number of examples
// bEntries is the number of bars
// Returns pointer to Prime
double *aTaMult(const double *b, int *rows, int *columns, int aEntries, int bEntries){
    double *bPrime = zeros(bEntries);
    
    for(int i=0; i<aEntries; i++){
        int row = rows[i];
        int column = columns[i];
        bPrime[row] += 2*(b[row] - b[column]);
        bPrime[column] += 2*(b[column] - b[row]);
    }

    return(bPrime);
}

// Compute the dot product of a and b
double dot(const double *a, const double *b, int entries){
    double output = 0.0;
    for(int i=0; i<entries; i++){
        output += a[i]*b[i];
    }
    return(output);
}

// Set a + b*alpha to c
void addMult(const double *a, const double *b, double *c, double alpha, int entries){
    for(int i=0; i<entries; i++){
        c[i] = a[i] + b[i]*alpha;
    }
}
